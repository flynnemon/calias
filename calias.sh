#! /bin/bash

# Path to bash profile
PROFILE=~/.bash_profile

# Create bash profile if not already there
touch $PROFILE

# Gather Data
echo "Name of alias to create:"
read SHORTCUT
echo "Command to run on alias execution:"
read COMMAND
echo "Description:"
read DESCRIPTION

# Write comment and alias to bash profile
echo "# "$DESCRIPTION >> $PROFILE
echo 'alias '$SHORTCUT'="'$COMMAND'"' >> $PROFILE

echo 'Run "source '$PROFILE'" and the alias "'$SHORTCUT'" wil be ready to use'