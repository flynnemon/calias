Tool for adding alias to bash

INSTALL:

	1. Clone repo
	2. cd calias/
	3. mkdir ~/.calias
	4. cp calias.sh ~/.calias/
	5. chmod +x ~/.calias/calias.sh
	6. touch ~/.bash_profile
	7. echo 'alias calias="~/.calias/calias.sh"' >> ~/.bash_profile